
# Normalization and Commands in SQL

The aim of this project is to get the reader familiar with concepts and commands used in SQL.

## SQL

The Structured Query Language is a domain-specific language used in programming, mainly essential in usage of structured data; which means that it is used for handling of data incorporating relations amidst variables and entities.
- It has been designed for managing data held in an RDBMS (relational database management system), or for stream processing in an RDBSMS (relational data stream management system). 
- It accesses many records in one command and eliminates the need to specify index to reach an individual record.
- ANSI and ISO Standardized!
## 1. SQL Commands --->
The main idea here is to implement CRUD, which stands for create, read, update and delete!!

Supposing we already have a database from which we want to select all values from a specific column, say Customers. Our in-built database consists of entries as shown below.

Tablename | Consumers | ItemsTypes | Employees | OrderDetails | Orders | Products | Suppliers | Shippers
--- |--- |--- |--- |--- |--- |--- |--- |---
**Records** | 89 | 7 | 9 | 499 | 200 | 88 | 4 | 33

To select all values from the database from the table Customers, we will do as shown below. Results after selecting “Customers” would consist of only the 91 records of the same.
- **SELECT * FROM Customers;**

In SQL, records are called rows, while fields are stated as columns. . . 
### Select Statement, Sorting:
- **SELECT column1, column2, ...**
- **FROM table_name;**


Selects specified columns

- **SELECT DISTINCT column1, column2, ...**
- **FROM table_name;**


Selects unique entries.

ORDER BY for sorted results (default: ascending). 
- **SELECT column1, column2, ...**
- **FROM table_name**
- **ORDER BY column1, column2, ... ASC|DESC;**


You can even select specific items using WHERE, OR, NOT and AND. 
### Insert, Update, Delete:
- **INSERT INTO table_name (column1, column2, column3, ...)**
- **VALUES (value1, value2, value3, ...);**
Inculcate new records by using INSERT INTO.

- **UPDATE <table_name>**
- **SET column1 = value1, column2 = value2, ...**
- **WHERE condition;**
Update entries using UPDATE, SET, and WHERE.

Delete records using:
- **DELETE FROM <table_name>**
- **WHERE <condition>;**


### Joins
A JOIN clause is used when needed to combine rows from two or more tables, based on a related column between them. 

INNER JOIN to select records with matching values in both tables.

LEFT JOIN to return all records from the left table, and matching records from right table.
- Result is 0 records from the right side, if there is no match.

RIGHT JOIN can be used vice-versa.
- **SELECT column_name(s)**
- **FROM table1**
- **LEFT JOIN table2**
- **ON table1.column_name = table2.column_name;**
Full Join and Self-join statement
- Use FULL JOIN keyword for all the records from both tables.
- SELF JOIN: a table that is joined with itself!

### Aggregations
There are five SQL in-built aggregate functions, which are: MIN, MAX, COUNT, SUM, and AVG

### count() 
Find number of occurrences of entries in a column.
- SELECT COUNT(column_name)
- FROM table_name
- WHERE condition;

### avg() 
Find the average.
- SELECT AVG(column_name)
- FROM table_name
- WHERE condition;

### sum(): 
Add all column-values.
- SELECT SUM(column_name)
- FROM table_name
- WHERE condition;

### min() :
find minima:
- SELECT MIN(column_name)
- FROM table_name
- WHERE condition;

### max() : 
Find maxima:
- SELECT MAX(column_name)
- FROM table_name
- WHERE condition;

### Select Into, Insert Into Select, is Null, and 
    
    + The SELECT INTO statement copies data from one table into a new table.
    + The INSERT INTO SELECT statement copies data from one table and inserts it into another table.
    + The INSERT INTO SELECT statement requires that the data types in source and target tables match.
    + The MySQL IFNULL() function lets you return an alternative value if an expression is NULL
    + The SQL Server ISNULL() function lets you return an alternative value when an expression is NULL

### SQL Database Commands

- Create a database using: Create database command.
- Delete a database using: Drop database command.
- Create table using: Create table command.
- Delete a table using: Drop table command.
- Delete all data inside a table using: TRUNCATE TABLE command.
- Add columns to your tables using: ALTER TABLE command with ADD <column name> <TYPE>
- Delete columns using: ALTER TABLE with DROP COLUMN command.
- While selecting items to be shown, change their display names by using AS keyword: SELECT <col1> AS <NewName> FROM <TableName>

## 2. Normalization in SQL --->

As a SQL developer deals with enormous amounts of data that are stored in different tables present inside multiple databases, it often becomes difficult to extract the data
if it is not organized. 

This brings us to the concept of using **Normalization**; wherein we can do two things:
1. Solve the problem of **data redundancy** by decreasing or eliminating redundant datasets
1. Organize the data by using various different forms.

### Types of Normal form

**1st Normal Form**

    - The table cells must be of a single value.
    - Eliminate repeating groups in individual tables.
    - Create a separate table for each set of related data.
    - Identify each set of related data with a primary key.

Definition: An entity is in the first normal form if it contains no repeating groups, that is, **no repeating columns.**
In 1NF relation, the order of tuples (rows) and attributes (columns) does not matter. 

**2nd Normal Form**

    - Remove Partial Dependencies.
    - Functional Dependency: The value of one attribute in a table is determined entirely by the value of another.
    - Partial Dependency: A type of functional dependency where an attribute is functionally dependent on only part of the primary key (primary key must be a composite key).

Definition: A relation is in 2NF if it is **in 1NF and every non-key attribute is fully dependent on each candidate key of the relation**.

**3rd Normal Form**

A relation is in third normal form if it is in 2NF and every non-key attribute of the relation is non-transitively dependent on each candidate key of the relation.
- Note: Non-transitively dependant is a condition in which an attribute (non-primary-key) is dependent on another attribute (non-primary-key) that is not part of the primary key.
**Boyce-Codd Normal Form (BCNF)**
 
- A relation is in Boyce-Codd Normal Form (BCNF) if every determinant is a candidate key. 

**4th Normal Form**
 
1. An entity is in Fourth Normal Form (4NF) when it meets the requirement of being in Third Normal Form (3NF) and additionally:
-  Has no multiple sets of multi-valued dependencies. In other words, 4NF states that no entity can have more than a single one-to-many relationship within an entity if the one-to-many attributes are independent of each other.
-  Fourth Normal Form applies to situations involving many-to-many relationships.
2. In relational databases, many-to-many relationships are expressed through cross-reference tables.

Definition: A table is in fourth normal form (4NF) if and only if it is in BCNF and contains no more than one multi-valued dependency.

**5th Normal Form**

A table is in the fifth normal form (5NF) or Project-Join Normal Form (PJNF) if it is in 4NF and it cannot have a lossless decomposition into any number of smaller tables.
 
## Credits and More . . .

## References

1.	[SQL on Wikipedia](https://en.wikipedia.org/wiki/SQL)
2.	[W3schools - SQL tutorials](https://www.w3schools.com/sql/default.asp)
3.  [Khan Academy- Practice sessions](https://www.khanacademy.org/computing/computer-programming/sql)
4.  [Simply Learn SQL](https://www.simplilearn.com/tutorials/sql-tutorial/what-is-normalization-in-sql)
5.  [DBMS Normalization](https://www.javatpoint.com/dbms-normalization)

## 🚀 About Me
Soon to be full stack developer...


## 🛠 Skills
Python, Java, C++, MySQL, PostGRES, HTML, CSS, Javascript...


## Lessons Learned

So today we have learnt about some commands in SQL; and secondly, we have learnt about what Normalization in SQL is; and thirdly, we have learnt about the various kinds of normalization forms in SQL.
